# merrin



## Getting started

To get merrin

```
docker pull registry.gitlab.inria.fr/dyliss/merrin/merrin:v1
docker run -it --rm -p 8888:8888 registry.gitlab.inria.fr/dyliss/merrin/merrin:v1
```

## Additional information
[https://github.com/bioasp/merrin-covert](https://github.com/bioasp/merrin-covert)
